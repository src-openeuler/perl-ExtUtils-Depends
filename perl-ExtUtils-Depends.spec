Name:           perl-ExtUtils-Depends
Version:        0.8001
Release:        1
Summary:        Easily build XS extensions that depend on XS extensions
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/ExtUtils-Depends
Source0:        https://cpan.metacpan.org/modules/by-module/ExtUtils/ExtUtils-Depends-%{version}.tar.gz
BuildArch:      noarch
# Module Build
BuildRequires:  coreutils
BuildRequires:  make
BuildRequires:  perl-generators
BuildRequires:  perl-interpreter
BuildRequires:  perl(Cwd)
BuildRequires:  perl(ExtUtils::MakeMaker) >= 7.44
# Module Runtime
BuildRequires:  perl(Carp)
BuildRequires:  perl(Config)
BuildRequires:  perl(Data::Dumper)
BuildRequires:  perl(DynaLoader)
BuildRequires:  perl(File::Find)
BuildRequires:  perl(File::Spec)
BuildRequires:  perl(IO::File)
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
# Test Suite
BuildRequires:  perl(base)
BuildRequires:  perl(Exporter)
BuildRequires:  perl(File::Path)
BuildRequires:  perl(File::Spec::Functions)
BuildRequires:  perl(File::Temp)
BuildRequires:  perl(FindBin)
BuildRequires:  perl(lib)
BuildRequires:  perl(Test::More)
# Dependencies
Requires:       perl(DynaLoader)

%description
This module tries to make it easy to build Perl extensions that use
functions and typemaps provided by other Perl extensions. This means
that a Perl extension is treated like a shared library that provides
also a C and an XS interface besides the Perl one.

%prep
%setup -q -n ExtUtils-Depends-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%{make_build}

%install
%{make_install}
%{_fixperms} -c %{buildroot}

%check
make test

%files
%doc Changes README
%{perl_vendorlib}/ExtUtils/
%{_mandir}/man3/ExtUtils::Depends.3*

%changelog
* Fri Jan 26 2024 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 0.8001-1
- Update package with version 0.8001

* Fri Jun 28 2022 tanyulong <tanyulong@kylinos.cn> 0.8000-2
- Improve the project according to the requirements of compliance improvement

* Wed Jun 10 2020 Perl_Bot <Perl_Bot@openeuler.org> 0.8000-1
- Specfile autogenerated by Perl_Bot
